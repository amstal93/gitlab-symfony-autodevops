# Database server ready?
mysql --connect_timeout 10 -u $DB_USER -h $DB_HOST -p"$DB_PASSWORD" -e "SHOW GRANTS" > /dev/null 2>&1 || exit 1

php bin/console doctrine:database:drop --force || exit 1
php bin/console doctrine:database:create || exit 1
php bin/console doctrine:schema:update --force || exit 1

mysql -u $DB_USER -h $DB_HOST -p"$DB_PASSWORD" $DB_NAME < /res/data.sql || exit 1

# Copy public app files to the volume shared with the nginx container
cp -r /symfony/public/ /var/www/html/
